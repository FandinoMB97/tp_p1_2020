package juego;

import java.awt.Color;
import java.awt.Image;
import java.util.ArrayList;// SE SUMA
import java.util.Random;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	private Entorno entorno;
// Variables y metodos propios de cada grupo
	Random rand = new Random();
	private int obs;
	private int malos;
	private int totalMon;
	private int cantBolas;
	private int vidas;
	private int vidasRey;
	private int puntaje;
	private Montana[] montana;
	private Princesa princesa;
	private Nube[] nube;
	private Piso[] piso;
	private Bola[] bolas;
	private Arbusto[] arbustos;
	private Rey rey;
	
	// Enemigos
	private ArrayList<Enemigo> enemigos;
	// Obstaculos
	private ArrayList<Obstaculo> obstaculos;
	// Moneda
	private ArrayList<Moneda> monedas;
	Image imagen;

	int nivel = 1;
	int tiempo = 0;
	int tick = 0;
	boolean contadorTicks;

	Juego() {
		// Inicializa el objeto entorno

		this.obs = 3;
		this.malos = 3;
		this.totalMon = 3;
		this.cantBolas = 3;

		enemigos = new ArrayList<Enemigo>();
		obstaculos = new ArrayList<Obstaculo>();
		monedas = new ArrayList<Moneda>();
		
		this.bolas = new Bola[cantBolas];

		this.entorno = new Entorno(this, "Super Elizabeth Sis - Grupo - v6", 800, 600);

		// Inicializar lo que haga falta para el juego
		// ...

		this.princesa = new Princesa(150, 384, 90, 70);

		this.piso = new Piso[4];
		for (int i = 0; i < this.piso.length; i++) 
		{
			this.piso[i] = new Piso(500 + (600 * i), 500);
		}

//MONEDA
		for (int i = 0; i < totalMon; i++) 
		{
			Moneda moneda = new Moneda(500 + (700 * i) + rand.nextInt(70), 50 + rand.nextInt(100), 30, 30);
			monedas.add(moneda);
		}

		this.arbustos = new Arbusto[4];
		for (int i = 0; i < this.arbustos.length; i++) 
		{
			this.arbustos[i] = new Arbusto(200 + (400 * i), 360);
		}
		this.vidas = 3;
		this.vidasRey=3;
// obstaculos ( suma elemntos al array)

		for (int i = 0; i < obs; i++) 
		{
			Obstaculo obstaculo = new Obstaculo(700 + (700 * i), 380, 90, 70);
			obstaculos.add(obstaculo);
		}

// ENEMIGOS ( suma elemntos al array)
		for (int i = 0; i < malos; i++) 
		{
			Enemigo enemigo = new Enemigo(700 + (400 * i) + rand.nextInt(300), 400, 90, 70);
			enemigos.add(enemigo);
		}

		this.nube = new Nube[2];
		for (int i = 0; i < this.nube.length; i++) 
		{
			this.nube[i] = new Nube(300 + (650 * i), 40+rand.nextInt(100));
		}

		this.montana = new Montana[3];
		for (int i = 0; i < this.montana.length; i++) 
		{
			this.montana[i] = new Montana(300 + (800 * i), 250, princesa.getX());
		}

		// Inicia el juego!
		this.entorno.iniciar();
		princesa.getLetsGo().start();
	}

	// metodo entre princesa y enemigo//
	boolean colisionPrincesaEnemigo(Enemigo enemigo) 
	{
		if (this.princesa.getY() + this.princesa.getAlto() / 2 > enemigo.getY() - enemigo.getAlto() / 2
				&& this.princesa.getX() + this.princesa.getAncho() / 2 > enemigo.getX() - enemigo.getAncho() / 2
				&& this.princesa.getY() - this.princesa.getAlto() / 2 < enemigo.getY() + enemigo.getAlto() / 2
				&& this.princesa.getX() - this.princesa.getAncho() / 2 < enemigo.getX() + enemigo.getAncho() / 2)
			return true;
		else
			return false;
	}

	// metodo colision entre princesa y obstaculo//
	boolean colisionPrincesaObst(Obstaculo obs) 
	{
		if (this.princesa.getY() + this.princesa.getAlto() / 2 > obs.getY() - obs.getAlto() / 2
				&& this.princesa.getX() + this.princesa.getAncho() / 2 > obs.getX() - obs.getAncho() / 2
				&& this.princesa.getY() - this.princesa.getAlto() / 2 < obs.getY() + obs.getAlto() / 2
				&& this.princesa.getX() - this.princesa.getAncho() / 2 < obs.getX() + obs.getAncho() / 2)

			return true;
		else
			return false;
	}

	// metodo colision entre Princesa y moneda
	boolean colisionPrincesaMoneda(Moneda mon) 
	{
		if (this.princesa.getY() + this.princesa.getAlto() / 2 > mon.getY() - mon.getAlto() / 2
				&& this.princesa.getX() + this.princesa.getAncho() / 2 > mon.getX() - mon.getAncho() / 2
				&& this.princesa.getY() - this.princesa.getAlto() / 2 < mon.getY() + mon.getAlto() / 2
				&& this.princesa.getX() - this.princesa.getAncho() / 2 < mon.getX() + mon.getAncho() / 2)
			return true;
		else

			return false;
	}
	
	// metodo entre princesa y enemigo//
	boolean colisionEnemigoEnemigo(Enemigo enemigo ) 
	{
		for (int i=0; i<enemigos.size();i++) {

			if (enemigos.get(i).getX() + enemigos.get(i).getAncho() / 2
					> enemigo.getX() - enemigo.getAncho() / 2
					&& enemigos.get(i).getX() - enemigos.get(i).getAncho() / 2
					< enemigo.getX() + enemigo.getAncho() / 2) {
				return true;
			}

		}
		return false;
	}

	void setnivel(int tiempo) 
	{
		if (tiempo % 1000 == 0 && tiempo < 4000) 
		{
			nivel += 1;	
		}
	}	 
	
	/**
	 * Durante el juego, el mtodo tick() ser ejecutado en cada instante y por lo
	 * tanto es el mtodo ms importante de esta clase. Aqu se debe actualizar el
	 * estado interno del juego para simular el paso del tiempo (ver el enunciado
	 * del TP para mayor detalle).
	 */

	// *****************TICKS INSTANTE DE TIEMPO***********************//

	public void tick() 
	{
		try {
			if (puntaje >= 200 || vidasRey<=0)
			{
				princesa.getVictory().start();
				entorno.dibujarImagen(Herramientas.cargarImagen("youWin.png"), (double) 30, (double) 30, (double) 0);
				return;
			}
			
			if (vidas <= 0) 
			{
				entorno.dibujarImagen(Herramientas.cargarImagen("theEnd.png"), (double) 30, (double) 30, (double) 0);
				princesa.getGameOver().start();
				return;
			}

		} catch (Exception e) {
		}

// CONTADOR DE TICKS//
		tiempo = tiempo + 1;

		if (contadorTicks == true) 
		{
			tick = tick + 1;
		}

//VELOCIDAD//
		setnivel(tiempo);

// FONDO // DIBUJA FONDO//
		entorno.dibujarImagen(Herramientas.cargarImagen("fondo5.png"), 400, 300, 0);
		

//MONTANAS // DIBUJA Y MUEVE MONTAÑAS//

		for (int i = 0; i < this.montana.length; i++) 
		{
			this.montana[i].mover();
			this.montana[i].dibujar(this.entorno);
			if (this.montana[i].getx() <= -300) 
			{
				this.montana[i].setX(1500);
			}
		}

//PISO	// DIBUJA PISO //

		for (int i = 0; i < this.piso.length; i++) 
		{
			this.piso[i].mover();
			this.piso[i].setV(nivel);
			this.piso[i].dibujar(this.entorno);
			if (this.piso[i].getx() <= -800) 
			{
				this.piso[i].setX(1500);
			}
		}

		// Procesamiento de un instante de tiempo

//PRINCESA		// MOVIENTO SOBRE EJE X DE PRINCESA//
		if (this.entorno.estaPresionada(this.entorno.TECLA_IZQUIERDA) && princesa.getX() > 20) 
		{
			this.princesa.moverizquierda();
		}

		if (this.entorno.estaPresionada(this.entorno.TECLA_DERECHA) && princesa.getX() < this.entorno.ancho() - 450) 
		{
			this.princesa.moverderecha();
		}

// SALTO PRINCESA
		if (this.entorno.estaPresionada(this.entorno.TECLA_ARRIBA) && princesa.isCaida() == false) 
		{
			princesa.setSalto(true);
		}

		if (princesa.isCaida() == true) 
		{
			this.princesa.moverabajo();
			if (princesa.getY() == 380) 
			{
				this.princesa.moverderecha();
				princesa.setCaida(false);
			}
		}

		if (princesa.isSalto() == true) 
		{
			this.princesa.moverarriba();
			if (princesa.getY() == 376) 
			{
				princesa.getJump().start();
			}
			if (princesa.getY() == 124) 
			{
				princesa.setSalto(false);
				princesa.setCaida(true);
			}
		}

// DIBUJA PRINCESA//
		this.princesa.dibujar(this.entorno);

//OBSTACULO // DIBUJA Y MUEVE OBSTACULOS//
		for (int i = 0; i < this.obstaculos.size(); i++) 
		{
			this.obstaculos.get(i).mover();
			this.obstaculos.get(i).setV(nivel);
			this.obstaculos.get(i).dibujar(this.entorno);
			if (this.obstaculos.get(i).getX() <= -50) 
			{
				this.obstaculos.get(i).setX(2000 + rand.nextInt(1000));		
			}
			
// COLISION PRINCESA OBSTACULO//
			if (this.colisionPrincesaObst(this.obstaculos.get(i)) && tick == 0) 
			{
				this.vidas = vidas - (1);
				this.princesa.perdervida();
				princesa.getDeath().start();
				this.princesa.setFantasma(true);
				contadorTicks = true;	
			}
		}

		if (tick > 250) 
		{
			contadorTicks = false;
			tick = 0;
			this.princesa.moverderecha();
		}

//ENEMIGOS	// DIBUJA Y MUEVE ENEMIGOS//
		for (int i = 0; i < this.enemigos.size(); i++) 
		{
			this.enemigos.get(i).setV(-nivel);
			this.enemigos.get(i).mover();
			this.enemigos.get(i).dibujar(this.entorno);
			if (this.enemigos.get(i).getX() <= -10) 
			{
				this.enemigos.remove(i);
				this.enemigos.add(new Enemigo(700 + (400) + rand.nextInt(800), 400, 90, 70));
			}
			
// COLISION PRINCESA-ENEMIGO//
			if (colisionPrincesaEnemigo(this.enemigos.get(i)) && vidas > 0 && tick == 0) 
			{
				this.enemigos.remove(i);
				Enemigo nuevo = new Enemigo(700 + (400) + rand.nextInt(800), 400, 90, 70);
				while(colisionEnemigoEnemigo(nuevo)==true){
					int posicion=nuevo.getX();
					nuevo.setX(posicion+20+nuevo.getAncho());
				}
				this.enemigos.add(nuevo);
				this.princesa.setFantasma(true);
				this.princesa.perdervida();
				princesa.getDeath().start();
				this.vidas = vidas - 1;
				this.princesa.perdervida();
				this.nivel = 1;
				contadorTicks = true;
			}

			if (tick > 100) 	
			{
				this.princesa.setFantasma(false);
				contadorTicks = false;
				tick = 0;
				this.princesa.moverderecha();		
			}

// COLISION BOLA-ENEMIGO//
			for (int b = 0; b < this.bolas.length; b++) 
			{
				if (this.bolas[b] != null) 
				{						
					if (this.enemigos.get(i).getX() - this.enemigos.get(i).getAncho()/2 <= this.bolas[b].getX() + this.bolas[b].getDiametro() /2 
							&& this.enemigos.get(i).getX() + this.enemigos.get(i).getAncho()/2 >= this.bolas[b].getX() + this.bolas[b].getDiametro() /2) {

						this.bolas[b] = null;
						this.enemigos.remove(i);
						Enemigo nuevo = new Enemigo(700 + (400) + rand.nextInt(800), 400, 90, 70);
						while(colisionEnemigoEnemigo(nuevo)==true){
							int posicion = nuevo.getX();
							nuevo.setX(posicion+20+nuevo.getAncho());		
						}
						this.enemigos.add(nuevo);
						this.puntaje = puntaje + 5;						
					}

				}
				if (this.bolas[b] != null&&this.rey!=null &&rey.getX()<600)
				{
					if (this.rey.getX() - this.rey.getAncho()/2 <= this.bolas[b].getX() + this.bolas[b].getDiametro() /2 
							&& this.rey.getX() + this.rey.getAncho()/2 >= this.bolas[b].getX() + this.bolas[b].getDiametro() /2) {

						this.bolas[b] = null;
						rey.getGolpe().start(); //algo anda mal con el sonido aca,suena solo la 1ra vez
						this.vidasRey--;
					}
				}
			}
		}

//DISPARO	// DIBUJA Y MUEVE BOLA//
		if (this.entorno.sePresiono(this.entorno.TECLA_ESPACIO) && this.princesa.isSalto() == false
				&& princesa.isCaida() == false)
		{
			
			boolean agregueBola = false;
			for (int i = 0; i < this.bolas.length && !agregueBola; i++) 
			{
				if (this.bolas[i] == null) 
				{
					princesa.getDisparo().start(); //algo anda mal con el sonido aca,suena solo la 1ra vez
					this.bolas[i] = this.princesa.Lanzada();
					agregueBola = true;
				}
			}
		
		}

		// Dibuja las bolas de fuego
		for (int i = 0; i < this.bolas.length; i++) 
		{
			if (this.bolas[i] != null) 
			{
				this.bolas[i].dibujar(this.entorno);
				this.bolas[i].mover();
			}
		}
		
		//rey
		if(puntaje>10&&this.rey==null) 
		{
			this.rey= new Rey(700 + (400) + rand.nextInt(800), 385, 90, 70);
		}

		if (puntaje>10&&rey!=null) 
		{
			if(this.rey.getX()>550) 
			{
				this.rey.mover();
			}
			this.rey.dibujar(entorno);
			rey.getRisa().start();
		}
		
		if (this.rey!=null&&this.rey.getX()>600) 
		{
			this.rey.setV(-1);
		}			
	
//ARBUSTOS//
		for (int i = 0; i < this.arbustos.length; i++) 
		{
			this.arbustos[i].mover();
			this.arbustos[i].setV(nivel);
			this.arbustos[i].dibujar(this.entorno);

			if (this.arbustos[i].getx() <= -400) 
			{
				this.arbustos[i].setX(1050);
			}
		}
		
//MONEDA//
		for (int i = 0; i < this.monedas.size(); i++) 
		{
			this.monedas.get(i).mover();
			this.monedas.get(i).dibujar(this.entorno);
			if (this.monedas.get(i).getX() < -10) 
			{
				this.monedas.remove(i);
				this.monedas.add(new Moneda(1500 + (700 * i) + rand.nextInt(70), 50 + rand.nextInt(100), 30, 30));
			}
			if (colisionPrincesaMoneda(this.monedas.get(i))) 
			{
				puntaje = puntaje + 1;
				this.monedas.remove(i);
				this.monedas.add(new Moneda(1500 + (700 * i) + rand.nextInt(70), 50 + rand.nextInt(100), 30, 30));
			}
		}
//NUBES	// DIBUJA Y MUEVE NUBES//
		for (int i = 0; i < this.nube.length; i++) 
		{
			this.nube[i].mover();
			this.nube[i].dibujar(this.entorno);
			if (this.nube[i].getx() <= -400) 
			{
				this.nube[i].setX(1050);
			}
		}

//VIDAS		// *********DIBUJA  VIDAS Y PUNTAJE*******************//
		entorno.dibujarImagen(Herramientas.cargarImagen("cora.gif"),50, 60, 0);
		if (vidas>=2) 
		{
			entorno.dibujarImagen(Herramientas.cargarImagen("cora.gif"),100, 60, 0);
		}
		if (vidas>=3) 
		{
			entorno.dibujarImagen(Herramientas.cargarImagen("cora.gif"),150, 60, 0);
		}

//VIDAS_Rey		// *********DIBUJA  VIDAS Y PUNTAJE*******************//
		if (this.rey!=null&&this.rey.getX()<600) {
				entorno.dibujarImagen(Herramientas.cargarImagen("reyCara.gif"),600, 100, 0);
				entorno.dibujarImagen(Herramientas.cargarImagen("cora.gif"),650, 100, 0);
				if (vidasRey>=2) {
				entorno.dibujarImagen(Herramientas.cargarImagen("cora.gif"),700, 100, 0);}
				if (vidasRey>=3) {
				entorno.dibujarImagen(Herramientas.cargarImagen("cora.gif"),750, 100, 0);}
		}
		
//PUNTAJE
		entorno.dibujarImagen(Herramientas.cargarImagen("pointgiratorio"+ ".gif"), 700, 60, 0);
		entorno.cambiarFont("Arial", 30, Color.LIGHT_GRAY);
		entorno.escribirTexto(Integer.toString(puntaje), 700 + 30, 60 +10);	
	}

}

