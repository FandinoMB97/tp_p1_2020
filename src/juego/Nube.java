package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Nube 
{
	private double x;
	private double y;
	private Image imagen;

	public Nube(double x, double y) 
	{
		this.x = x;
		this.y = y;
		this.imagen = Herramientas.cargarImagen("nube3.png");
	}

	public void dibujar(Entorno e) 
	{
		e.dibujarImagen(imagen, this.x, this.y, 0);
	}

	public void mover() 
	{
		this.x -= 0.2;
	}

	public double getx() 
	{
		return this.x;
	}

	public void setX(int i) 
	{
		this.x = i;
	}
}