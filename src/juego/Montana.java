package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Montana {
	private double x;
	private double y;
	private double velocidad;
	private Image imagen;

	public Montana(double x, double y, double velocidad) 
	{
		this.x = x;
		this.y = y;
		this.imagen = Herramientas.cargarImagen("pico.png");
	}

	public void dibujar(Entorno e) 
	{
		e.dibujarImagen(imagen, this.x, this.y, 0);
	}

	public void mover() 
	{
		this.x -= 1+velocidad;
	}

	public double getx() 
	{
		return x;
	}

	public void setX(int i) 
	{
		this.x = i;
	}
	
	public void setV(int i) 
	{
		this.velocidad = i;
	}
}