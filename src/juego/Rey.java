package juego;

import java.awt.Image;

import javax.sound.sampled.Clip;

import entorno.Entorno;
import entorno.Herramientas;

public final class Rey {
	private int x;
	private int y;
	private int alto;
	private int ancho;
	private Image imagen;
	private Clip risaRey;
	private Clip golpe;
	private int velocidad;

	public Rey(int x, int y, int alto, int ancho) 
	{
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.imagen = Herramientas.cargarImagen("rey.gif");
		this.velocidad = 0;
		this.risaRey = Herramientas.cargarSonido("risa.wav");
		this.golpe = Herramientas.cargarSonido("golpe.wav");
	}

	public void dibujar(Entorno e) 
	{
		e.dibujarImagen(imagen, this.x, this.y, 0);
	}

	public void mover() 
	{
		this.x -= 2- velocidad;
	}

	public int getX() 
	{
		return x;
	}

	public int getY() 
	{
		return y;
	}

	public int getAlto() 
	{
		return alto;
	}

	public int getAncho() 
	{
		return ancho;
	}

	public void setX(int x) 
	{
		this.x = x;
	}
	
	public void setV(int x) 
	{
		this.velocidad = x;
	}

	public Clip getRisa() 
	{
		return risaRey;
	}

	public Clip getGolpe()
	{
		return golpe;
	}
}
