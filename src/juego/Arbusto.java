package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Arbusto {
	private double x;
	private double y;
	private Image imagen;
	private int velocidad;
	private int stop;
	
	public Arbusto(double x, double y) 
	{
		this.x = x;
		this.y = y;
		this.imagen = Herramientas.cargarImagen("arbusto (2).png");
		this.velocidad= 0;
		this.stop=1;
	}

	public void dibujar(Entorno e) 
	{
		e.dibujarImagen(imagen, this.x, this.y, 0);
	}

	public void mover()
	{
		this.x -=( 1 + velocidad);
	}

	public void setV(int i)
	{
		this.velocidad= i;
	}
	
	public void setStop(int i) 
	{
		this.stop=i;
	}
	public double getx() 
	{
		return this.x;
	}

	public void setX(double i) 
	{
		this.x= i;	
	}

}
