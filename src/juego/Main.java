package juego;

import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Main extends JFrame implements  ActionListener{
	JButton boton1 = new JButton("jugar");
	JButton boton2 = new JButton("salir");
	JPanel panel = new JPanel();
	
	public Main() {
		this.setSize(800, 600);
		this.setTitle("Super Elisabeth");
		this.setLocationRelativeTo(null);
		iniciarComponentes ();
		iniciarBotones ();
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);			
	}
	private void iniciarComponentes(){		
		panel.setLayout(null);
		this.getContentPane().add(panel);
		JLabel fondo = new JLabel (new ImageIcon("recursos/fondoMenu.gif"));
		fondo.setBounds(0,0, 800, 600);
		panel.add(fondo);
	}	
	private void iniciarBotones(){			
		boton1.setBounds(300, 200, 150, 40);
		ImageIcon jugar = new ImageIcon("recursos/JUGAR.png");
		boton1.setIcon(new ImageIcon (jugar.getImage().getScaledInstance(boton1.getWidth(), boton1.getHeight(), Image.SCALE_SMOOTH)));
		boton1.setFont(new Font("cooper black",Font.ITALIC,25));
		panel.add(boton1);
		boton1.addActionListener ((ActionListener) this);
		
		boton2.setBounds(300, 250, 150, 40);
		ImageIcon instruc = new ImageIcon("recursos/SALIR.png");
		boton2.setIcon(new ImageIcon (instruc.getImage().getScaledInstance(boton2.getWidth(), boton2.getHeight(), Image.SCALE_SMOOTH)));
		panel.add(boton2);
		boton2.addActionListener ((ActionListener) this);	
	}
	
		public void actionPerformed (ActionEvent e){	
		if (e.getSource().equals(boton1)){
				new Juego();
		}
		if (e.getSource().equals(boton2)){
			this.dispose(); 	
	}
}
	
	public static void main(String[] args) {
		Main v1 = new Main();
		v1.setVisible(true);
	}
}
