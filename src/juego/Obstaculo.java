package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public final class Obstaculo {
	private double x;
	private double y;
	private double alto;
	private double ancho;
	private Image imagen;
	private int velocidad;

	public Obstaculo(double x, double y, double alto, double ancho) 
	{
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.imagen = Herramientas.cargarImagen("tubo.png");
		this.velocidad=0;
	}

	public void dibujar(Entorno e) 
	{
		e.dibujarImagen(imagen, this.x, this.y, 0);
	}

	public void mover() 
	{
		this.x -= 1+velocidad;
	}

	public double getX() 
	{
		return x;
	}

	public void setX(int x) 
	{
		this.x = x;
	}

	public double getY() 
	{
		return y;
	}

	public void setY(double y) 
	{
		this.y = y;
	}

	public double getAlto() 
	{
		return alto;
	}

	public double getAncho() 
	{
		return ancho;
	}

	public void setV(int i) 
	{
		this.velocidad= i;
	}

}
