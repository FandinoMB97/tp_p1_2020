package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Moneda {
	private double x;
	private double y;
	private double alto;
	private double ancho;
	private Image imagen;

	public Moneda(double x, double y, double alto, double ancho) 
	{
		this.x = x;
		this.y = y;
		this.alto = alto;
		this.ancho = ancho;
		this.imagen = Herramientas.cargarImagen("pointgiratorio.gif");
	}

	public void dibujar(Entorno e) 
	{
		e.dibujarImagen(imagen, this.x, this.y, 0);
	}

	public void mover() 
	{
		this.x -= 2.5;
	}

	public double getY() 
	{
		return y;
	}

	public void setX(int i) 
	{
		this.x = i;
	}

	public double getX() 
	{
		return x;
	}

	public double getAlto() 
	{
		return this.alto;
	}

	public double getAncho() 
	{
		return this.ancho;
	}
}
