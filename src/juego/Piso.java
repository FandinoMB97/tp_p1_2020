package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public final class Piso {
	private double x;
	private double y;
	private Image imagen;
	private int velocidad;
	
	public Piso(double x, double y) 
	{
		this.x = x;
		this.y = y;
		this.imagen = Herramientas.cargarImagen("suelo2).png");
		this.velocidad=0;;
	}

	public void dibujar(Entorno e) 
	{
		e.dibujarImagen(imagen, this.x, this.y, 0);
	}

	public void mover() 
	{
		this.x -= 1+velocidad;
	}

	public double getx() 
	{
		return this.x;
	}

	public void setX(int i)
	{
		this.x = i;
	}
	
	public void setV(int i) 
	{
		this.velocidad= i;
	}

}
